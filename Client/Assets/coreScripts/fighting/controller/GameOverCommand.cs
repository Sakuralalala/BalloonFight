﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.fighting.model;
using Assets.coreScripts.fighting.util;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.controller
{
    public class GameOverCommand : EventCommand
    {

        [Inject]
        public IGameTimer timer { get; set; }
        public override void Execute()
        {
            GameConfig.PlayerState state = (GameConfig.PlayerState)evt.data;


            switch (state)
            {
                case GameConfig.PlayerState.NORMAL:
                    break;
                case GameConfig.PlayerState.DIE:
                    CustomPropsEventData d = new CustomPropsEventData();
                    d.type = GameConfig.PropsState.PROPS_COMMAND;
                    d.propStatus = GameConfig.PropsState.LOSE;
                    dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d);
                    break;
                case GameConfig.PlayerState.GET_HIT:
                    break;
                case GameConfig.PlayerState.WINNER:
                   CustomPropsEventData d1 = new CustomPropsEventData();
                    d1.type = GameConfig.PropsState.PROPS_COMMAND;
                    d1.propStatus = GameConfig.PropsState.WIN;
                    dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d1);
                    break;
                case GameConfig.PlayerState.LOUSER:
                    CustomPropsEventData d2 = new CustomPropsEventData();
                    d2.type = GameConfig.PropsState.PROPS_COMMAND;
                    d2.propStatus = GameConfig.PropsState.LOSE;
                    dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d2);
                    break;
                default:
                    CustomPropsEventData d3= new CustomPropsEventData();
                    d3.type = GameConfig.PropsState.PROPS_COMMAND;
                    d3.propStatus = GameConfig.PropsState.LOSE;
                    dispatcher.Dispatch(GameConfig.PropsState.PROPS_COMMAND, d3);
                    break;
            }

        }
    }
}
