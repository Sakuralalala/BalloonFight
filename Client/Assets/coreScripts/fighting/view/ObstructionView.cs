﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.fighting.model;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class ObstructionView:EventView
    {
        public GameConfig.ObstructionModel Model = GameConfig.ObstructionModel.BOUNCE;
        public GameConfig.Direction ForceDir = GameConfig.Direction.UP;
        private float force = 20;
        public float CustomForce = 0;

        private void OnHit(GameObject o) 
        {
            //if (tag.ToUpper() == "ENEMY" || tag.ToUpper() == "PLAYER")
            //{
            //    return;
            //}
            switch (Model)
            {
                case GameConfig.ObstructionModel.PLAYER_BALLON_HURT:
                    if (o.tag.ToUpper() != "ENEMY")
                        return;
                    break;
                case GameConfig.ObstructionModel.ENEMY_BALLON_HURT:
                    if (o.tag.ToUpper() != "PLAYER")
                        return;
                    break;
                case GameConfig.ObstructionModel.OUTREEL_DIE:
                case GameConfig.ObstructionModel.DRAPSEA_DIE:
                case GameConfig.ObstructionModel.ENTER_BOSSSTAGE:
                    if (o.tag.ToUpper()!="PLAYER")
                        return;
                    break;
                case GameConfig.ObstructionModel.NORMAL:
                case GameConfig.ObstructionModel.HURT_BOUNCE:
                case GameConfig.ObstructionModel.BOUNCE:
                    if (o.tag.ToUpper() != "PLAYER" && o.tag.ToUpper() != "ENEMY")
                        return;
                    break;
                case GameConfig.ObstructionModel.GOLD:
                    if (o.tag.ToUpper() != "PLAYER" && o.tag.ToUpper() != "PLAYERBALLOON")
                        return;
                    break;
                default:
                    Debug.Log("default ...");
                    return;
            }
            

            CustomHitEventData data = new CustomHitEventData();
            data.type = GameConfig.ObstructionModel.ON_HIT;
            data.currentObj = gameObject;
            data.targetObj = o;
            data.hitModel = Model;
            data.forceDir = ForceDir;
            data.force = this.force;

            dispatcher.Dispatch(GameConfig.ObstructionModel.ON_HIT, data);
        }

        //public void OnTriggerStay2D(Collider2D other)
        //{
        //    if (other.gameObject.tag.ToUpper() != "PLAYER")
        //    {
        //        return;
        //    }
        //    if (other.rigidbody2D)
        //    {
        //        other.rigidbody2D.velocity = new Vector2(-GameConfig.REEL_SPEED, 0);//new Vector2(Mathf.Clamp(data.targetObj.rigidbody2D.velocity.x - GameConfig.REEL_SPEED, GameConfig.REEL_SPEED, 10), data.targetObj.rigidbody2D.velocity.y);
        //    }
        //}

        /// <summary>
        /// 气球碰撞 金币碰撞
        /// </summary>
        /// <param name="other"></param>
        public void OnTriggerEnter2D(Collider2D other)
        {
            //coll.rigidbody.AddForce()
            if (CustomForce > 0)
                force = CustomForce;

            OnHit(other.gameObject);

            //if (other.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString())
            //{

            //    switch (Model)
            //    {
            //        case GameConfig.ObstructionModel.GOLD:
            //            GameObject.Destroy(gameObject);
            //            return;
            //        case GameConfig.ObstructionModel.ENTER_BOSSSTAGE:
            //            CoreLogicCtrl.Instance.ShowBoss();
            //            return;
            //        default:
            //            break;
            //    }
            //}
        }



        /// <summary>
        /// 平台碰撞 墙壁碰撞   
        /// </summary>
        /// <param name="coll"></param>
        public void OnCollisionEnter2D(Collision2D coll)
        {
            //coll.rigidbody.AddForce()
            if (CustomForce > 0)
                force = CustomForce;


            OnHit(coll.gameObject);


            //if (coll.gameObject.tag.ToUpper() == GameConfig.BallonType.PLAYER.ToString())
            //{

            //    switch (Model)
            //    {
            //        case GameConfig.ObstructionModel.OUTREEL_DIE:
            //            data.hitModel = GameConfig.ObstructionModel.OUTREEL_DIE;
            //            //coll.gameObject.SendMessage("OnPlayerDie", data, SendMessageOptions.RequireReceiver);
            //            return;
            //        case GameConfig.ObstructionModel.DRAPSEA_DIE:
            //            data.hitModel = GameConfig.ObstructionModel.DRAPSEA_DIE;
            //            //coll.gameObject.SendMessage("OnPlayerDie", data, SendMessageOptions.RequireReceiver);
            //            return;
            //        case GameConfig.ObstructionModel.HURT:
            //            CoreLogicCtrl.Instance.AttackUser(.2f);
            //            return;
            //        case GameConfig.ObstructionModel.HURT_BOUNCE:
            //            CoreLogicCtrl.Instance.AttackUser(.2f);
            //            break;
            //        default:
            //            break;
            //    }
            //}
            //else { return; }


            //if (!coll.rigidbody)
            //    return;

            //Debug.Log(coll.gameObject.name + "    " + gameObject.name);

            //switch (Dir)
            //{
            //    case GameConfig.Direction.UP:
            //        coll.rigidbody.AddForce(Vector2.up * force, ForceMode2D.Impulse);
            //        break;
            //    case GameConfig.Direction.DOWN:
            //        coll.rigidbody.AddForce(-Vector2.up * force, ForceMode2D.Impulse);
            //        break;
            //    case GameConfig.Direction.LEFT:
            //        coll.rigidbody.AddForce(-Vector2.right * force, ForceMode2D.Impulse);
            //        break;
            //    case GameConfig.Direction.RIGHT:
            //        coll.rigidbody.AddForce(Vector2.right * force, ForceMode2D.Impulse);
            //        break;
            //    default:
            //        break;
            //}

        }
    }
}
