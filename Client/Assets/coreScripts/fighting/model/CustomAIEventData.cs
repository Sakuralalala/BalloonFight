﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.coreScripts.fighting.model
{
    public class CustomAIEventData : CustomEventData
    {
        public GameConfig.AIEventType ai { get; set; }
        public GameConfig.EnemyLevel enemyLevel { get; set; }
        public float enemyMaxFly { get; set; }
        public float enemyMinFly { get; set; }
        public GameConfig.Direction enemyFlyHRDir { get; set; }
        public GameConfig.Direction enemyFlyVHDir { get; set; }
    }
}
